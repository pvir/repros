Artifacts in FFMPEG aacenc with bit_rate_tolerance=0
====================================================

```
ffmpeg -i input.wav -c aac -b:a 320k -bt:a 0 bad.aac

ffmpeg -i input.wav -c aac -b:a 96k good.aac
```

There's an audible artifact in the violin high note sound at 0:01-0:02 in
`bad.aac` encoded with `bit_rate_tolerance=0`, and "cheep-cheep" echoes after
that.  The average bitrate of `bad.aac` is ~166k.

The artifact is not present in the lower bitrate `good.aac`, or in the input
files.

The output with `bit_rate_tolerance=0` on other inputs appears to also contain
other instances of artifacts / bad quality, so it doesn't seem production ready
right now. I'd like to use it for Bluetooth A2DP AAC encoding, which needs such
frame length limiter, but in current state that's probably not a good idea.

The situation appears to be the same with libavcodec 60.31.102 and current
master (commit a15d2fdfd96c0).

```
ffmpeg -i nonterminating.flac -c aac -b:a 128k -bt:a 0 nonterminating.aac
```

The encoder gets into a non-terminating loop, as the frame length stops
reducing, and `s->lambda` goes beyond `FLT_EPSILON`.  Apparently, for some
inputs it's not possible to get a small enough frame for any value of
`s->lambda`.

The encoder probably should just give up in this case and produce a too big
frame.  The caller should then always check if the frame produced satisfied the
constraint.  Or, the encoder should bail out and return error.

```
ffmpeg -y -f loas -c aac_latm -i dump.loas dump.wav
```

The `dump.loas` is produced by

1. Have PipeWire from the `libav` branch (below) running
2. `btmon -w dump.log`
3. Connect Sony LinkBuds S
4. Play audio sample
5. Ctrl-C btmon
6. `./btsnoop-aacdec dump.log`

PipeWire was linked with libavcodec from FFMPEG master branch (commit
a15d2fdfd9).  Same audio as `bad.aac`, but the AAC encoding artifacts for some
reason sound much worse (there's high-frequency "ringing") on this headset than
on my another audio interface.

Observed on:
- ffmpeg version 6.1.2
- ffmpeg N-116991-ga15d2fdfd9

Links:
- https://ffmpeg.org/pipermail/ffmpeg-devel/2023-April/308861.html
- https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/4246
- https://gitlab.freedesktop.org/pvir/pipewire/-/commits/libav
